- name: Set up facts
  set_fact:
    k8s_cluster: "{{ lookup('env', 'K8S_CLUSTER_CONTEXT')}}"
    k8s_namespace: "{{ lookup('env', 'ENVIRONMENT')}}"
    docker_registry: "{{ lookup('env', 'DOCKER_REGISTRY')}}"
    docker_repo_prefix: "{{ lookup('env', 'DOCKER_REPO_PREFIX')}}"
    app_name: "{{ lookup('env', 'APP_NAME') | lower }}"
    app_version: "{{ lookup('env', 'APP_VERSION') | lower }}"
    app_port: "{{ lookup('env', 'APP_PORT') | int }}"
    app_environment: "{{ lookup('env', 'ENVIRONMENT') | lower }}"
    app_is_domain_root: "{{ lookup('env', 'DOMAIN_ROOT') | lower }}"
    app_base_domain: "{{ lookup('env', 'APP_BASE_DOMAIN') | lower }}"
    app_subdomain: "{{ lookup('env', 'APP_SUBDOMAIN') | lower }}"
    public_facing: "{{ lookup('env', 'PUBLIC_FACING') }}"
    app_resources:
      replicas: "{{ lookup('env', 'K8S_MIN_REPLICAS') }}"
      requests:
        cpu: "{{ lookup('env', 'K8S_MIN_CPU') }}"
        memory: "{{ lookup('env', 'K8S_MIN_MEMORY') }}"
      limits:
        cpu: "{{ lookup('env', 'K8S_MAX_CPU') }}"
        memory: "{{ lookup('env', 'K8S_MAX_MEMORY') }}"
    has_workers: "{{ lookup('env', 'HAS_WORKERS') }}"
    worker_resources:
      replicas: "{{ lookup('env', 'K8S_MIN_WORKER_REPLICAS') }}"
      requests:
        cpu: "{{ lookup('env', 'K8S_MIN_WORKER_CPU') }}"
        memory: "{{ lookup('env', 'K8S_MIN_WORKER_MEMORY') }}"
      limits:
        cpu: "{{ lookup('env', 'K8S_MAX_WORKER_CPU') }}"
        memory: "{{ lookup('env', 'K8S_MAX_WORKER_MEMORY') }}"

- name: Render compound facts
  set_fact:
    app_configmap_name: "{{ app_name }}-{{ app_environment }}-config"
    app_secretmap_name: "{{ app_name }}-{{ app_environment }}-config"
    service_subdomain: "{{ app_subdomain }}{% if app_environment != 'prod' %}-{{ app_environment }}{% endif %}"

- name: Deploy the application
  community.kubernetes.k8s:
    state: present
    wait: yes
    name: "{{ app_name }}"
    context: "{{ k8s_cluster }}"
    namespace: "{{ k8s_namespace }}"
    template: k8sapplication.yml

- name: Register deployment as service
  community.kubernetes.k8s:
    state: present
    wait: yes
    name: "{{ app_name }}"
    context: "{{ k8s_cluster }}"
    namespace: "{{ k8s_namespace }}"
    template: k8sservice.yml

- name: Build TLS host list
  when: app_is_domain_root == true
  set_fact:
    ingress_hosts:
      - "{{ app_base_domain }}"
      - "www.{{ app_base_domain }}"

- name: Build TLS host list
  when: app_is_domain_root == false
  set_fact:
    ingress_hosts:
      - "{{ service_subdomain }}.{{ app_base_domain }}"

- name: Register application in ingress controller
  when: public_facing == true
  community.kubernetes.k8s:
    state: present
    wait: yes
    name: "{{ app_name }}-{{ app_environment}}"
    template: k8singressregistration.yml

- name: Deploy the workers
  when: has_workers == true
  community.kubernetes.k8s:
    state: present
    wait: yes
    name: "{{ app_name }}-worker"
    context: "{{ k8s_cluster }}"
    namespace: "{{ k8s_namespace }}"
    template: k8sworker.yml
